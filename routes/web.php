<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

// $router->get('/', function () use ($router) {
//     return $router->app->version();
// });


$router->get('/' , 'WelcomeController@index');


$router->group(['prefix' => ''] , function () use ($router) {

    $router->get('signup' , [
        'as' => 'signup.index', 'uses' => 'SignupController@index'
    ]);

    $router->post('signup' , [
        'as' => 'signup.store', 'uses' => 'SignupController@store'
    ]);

    $router->get('signup/create' , [
      'as' => 'signup.create', 'uses' =>  'SignupController@create'
    ]);
});


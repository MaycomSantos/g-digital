<?php

use Illuminate\Database\Seeder;

class SignupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('signup_models')->insert([
            [
                'name'              => 'Michael Santos',
                'email'             => 'michaelsantos.the@hotmail.com',
                'phone'             =>  '86 9910-6102',
            ],
            ]);
    }
}

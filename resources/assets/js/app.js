window.Vue = require('vue');

Vue.component('example', require('./components/ExampleComponent.vue'));
Vue.component('signup', require('./components/Signup.vue'));

const app = new Vue({
    el: '#app'
});

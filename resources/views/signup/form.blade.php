<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <title>Sign Up - Tivo - SaaS App HTML Landing Page Template</title>


    <link href="/https://fonts.googleapis.com/css?family=Open+Sans:400,400i,700&display=swap&subset=latin-ext" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/fontawesome-all.css" rel="stylesheet">
    <link href="/css/swiper.css" rel="stylesheet">
	<link href="/css/magnific-popup.css" rel="stylesheet">
	<link href="/css/styles.css" rel="stylesheet">


    <link rel="icon" href="images/favicon.png">
</head>
        <body data-spy="scroll" data-target=".fixed-top">


            <div class="spinner-wrapper">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>



    <header id="header" class="ex-2-header">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Cadastrar</h1>


                    <div class="form-container">
                        <form action="{{ route('signup.store') }}" method="POST">

                        <div class="form-group">
                                <input type="text" class="form-control-input" name="name" id="name">
                                <label class="label-control" for="name">Nome</label>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <input type="email" class="form-control-input" name="email" id="email">
                                <label class="label-control" for="email">Email</label>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-control-input" name="phone" id="phone">
                                <label class="label-control" for="phone">Telefone</label>
                                <div class="help-block with-errors"></div>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="form-control-submit-button">Cadastrar</button>
                            </div>
                            <div class="form-message">
                                <div id="smsgSubmit" class="h3 text-center hidden"></div>
                            </div>
                        </form>
                    </div>


                </div>
            </div>
        </div>
    </header>

        <script src="/js/jquery.min.js"></script>
        <script src="/js/popper.min.js"></script>
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/jquery.easing.min.js"></script>
        <script src="/js/swiper.min.js"></script>
        <script src="/js/jquery.magnific-popup.js"></script>
        <script src="/js/validator.min.js"></script>
        <script src="/js/scripts.js"></script>
</body>
</html>

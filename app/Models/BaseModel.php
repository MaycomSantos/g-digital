<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    //Essa base model é usada como um escopo geral para buscar qualquer informação de acordo com o Modelo que está requisitando

     /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 50;

    /**
     * The array fields to filter in search query.
     *
     * @var array
     */
    protected $filters = [];

    /**
     * Create a new Eloquent query builder for the model.
     *
     * @param  \Illuminate\Database\Query\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder|static
     */
    public function newEloquentBuilder($query)
    {
        $builder = new \App\Builders\BaseBuilder($query);
        $builder->setFilters($this->filters);

        return $builder;
    }
}

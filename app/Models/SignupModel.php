<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Casts\Phone;

class SignupModel extends BaseModel
{
    protected $fillable = ['name','email','phone','details'];

    protected $casts = [
        'phone' => Phone::class,
    ];
}

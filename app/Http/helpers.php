<?php

use Illuminate\Support\Arr;

/*
|--------------------------------------------------------------------------
| Base Helpers
|--------------------------------------------------------------------------
*/


if (! function_exists('phone_db')) {

    /**
     * Formata campo de telefone para salvar no banco de dados.
     *
     * @param string $value
     * @return string
     */
    function phone_db($value)
    {
        return str_replace(['(', ')', '-', ' '], [''], $value);
    }
}

if (! function_exists('phone_view')) {

   /**
     * Formata campo de telefone para exibir nas views.
     *
     * @param string $value
     * @return string
     */
    function phone_view($value)
    {
        if (strlen($value) == 10) {
            return preg_replace("/(\d{2})(\d{4})/", "(\$1) \$2-\$3", $value);
        }

        return preg_replace("/(\d{2})(\d{1})(\d{4})/", "(\$1) \$2 \$3-\$4", $value);
    }
}

/*
|--------------------------------------------------------------------------
| Validation Helpers
|--------------------------------------------------------------------------
*/

if (! function_exists('valid_phone')) {

    /**
     * validador do campo telefone.
     *
     * @param string $number
     * @return string
     */
    function valid_phone($number)
    {
        return preg_match('/^(?:(?:\+|00)?(55)\s?)?(?:\(?([1-9][0-9])\)?\s?)?(?:((?:9\d|[2-9])\d{3})\-?(\d{4}))$/', $number);
    }
}

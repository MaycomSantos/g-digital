<?php

namespace App\Http\Requests;

use Urameshibr\Requests\FormRequest;


class SignupRequest extends FormRequest

{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'      => 'required',
            'email'     => 'required|email|unique:users',
            'phone'     =>  'required','min:10',
            'details'   =>  'nullable'
        ];
    }

    public function message()
     {
        return [
            'name'      =>  'O Nome é obrigatório',
            'email'     =>  'O Email é obrigatório',
            'phone'     =>  'Telefone é obrigatório',
        ];
    }
    protected function prepareForValidation()
    {
        $this->merge([
            'phone' => phone_db($this->phone),
        ]);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SignupRequest;
use App\Http\Controllers\Controller;

use App\Models\SignupModel;



class SignupController extends Controller
{

    public function index()
    {
        $signup = Signup::search()
            ->orderBy('name')
            ->paginate(20);

        return view('signup.index', compact('signup'));
    }

    public function create()
    {
        return view('signup.form');
    }

    public function store(SignupRequest $request)
    {
         Signup::create($request->all());

        return redirect('/signup')
            ->withSuccess('Cadastro realizado com sucesso');
    }
    public function show(Signup $signup)
    {
        return view('signup.show' , compact('signup'));
    }

    public function edit(Signup $signup)
    {
        return view('signup.form', compact('signup'));
    }

    public function update(SignupRequest $request, Signup $signup)
    {
        //
    }

    public function destroy(Signup $signup)
    {
        //
    }
}
